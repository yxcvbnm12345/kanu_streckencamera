import configparser

CONFIG_MAIN = "Main"
CONFIG_CAMERA1 = "Camera1"
CONFIG_CAMERA2 = "Camera2"

config = configparser.RawConfigParser()
config.read('cam.cfg')


def readConfig(section, name):
    return config.get(section, name)


def writeConfig(section, name, value, save=True):
    config[section][name] = value
    if save:
      saveConfig()


def saveConfig():
    with open('cam.cfg', 'w') as configfile:
        config.write(configfile)


def main():
    config.add_section(CONFIG_MAIN)
    writeConfig(CONFIG_MAIN, "ip", "192.168.1.100", False)
    writeConfig(CONFIG_MAIN, "imWidth", "2592", False)
    writeConfig(CONFIG_MAIN, "imHeight", "1944", False)
    writeConfig(CONFIG_MAIN, "imFPS", "2", False)

    config.add_section(CONFIG_CAMERA1)
    writeConfig(CONFIG_CAMERA1, "port", "5577", False)

    config.add_section(CONFIG_CAMERA2)
    writeConfig(CONFIG_CAMERA2, "port", "5578", False)
    saveConfig()


if __name__ == '__main__':
    main()
