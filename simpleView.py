# import libraries
from vidgear.gears.asyncio import NetGear_Async
import cv2, asyncio
import config

port = config.readConfig(config.CONFIG_CAMERA1, "port")
client = NetGear_Async(receive_mode=True, port=port, logging=True).launch()

async def main():
    async for frame in client.recv_generator():
        cv2.imshow("Output Frame", frame)
        key = cv2.waitKey(1) & 0xFF
        await asyncio.sleep(0)


if __name__ == "__main__":
    asyncio.set_event_loop(client.loop)
    try:
        client.loop.run_until_complete(main())
    except (KeyboardInterrupt, SystemExit):
        pass

    cv2.destroyAllWindows()
    client.close()