from turtle import width
from vidgear.gears import CamGear
from vidgear.gears.asyncio import NetGear_Async
import config
import asyncio


class CameraServer:
    def __init__(self, sourceId, confSection):
        self.sourceId = sourceId
        self.confSection = confSection

        width = int(config.readConfig(config.CONFIG_MAIN, "imWidth"))
        height = int(config.readConfig(config.CONFIG_MAIN, "imHeight"))
        fps = int(config.readConfig(config.CONFIG_MAIN, "imFPS"))
        options = {
            "CAP_PROP_FRAME_WIDTH": width,
            "CAP_PROP_FRAME_HEIGHT": height,
            "CAP_PROP_FPS": fps,
        }
        serverAddress = config.readConfig(config.CONFIG_MAIN, "ip")
        port = int(config.readConfig(self.confSection, "port"))
        self.stream = CamGear(source=self.sourceId, **options).start()
        self.server = NetGear_Async(
            source=None, address=serverAddress, port=port, pattern=2, logging=True)

        asyncio.set_event_loop(self.server.loop)
        self.server.config["generator"] = self.frame_generator()
        self.server.launch()

    def __del__(self):
        if self.stream:
            self.stream.stop()
        if self.server:
            self.server.close()

    async def frame_generator(self):
        while True:
            frame = self.stream.read()
            if frame is None:
                break
            yield frame
            await asyncio.sleep(0)
